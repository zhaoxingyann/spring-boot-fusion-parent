package org.spring.boot.fusion.feign.config;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Feign;
import feign.Retryer;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient.Builder;

@Configuration
@ConditionalOnClass(Feign.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
public class FusionFeginHttpClientConfig {

	@Bean
	public okhttp3.OkHttpClient okHttpClient() {
		Builder newBuilder = new okhttp3.OkHttpClient().newBuilder();
		// 设置连接超时
		newBuilder.connectTimeout(10, TimeUnit.SECONDS);
		// 设置读超时
		newBuilder.readTimeout(10, TimeUnit.SECONDS);
		// 设置写超时
		newBuilder.writeTimeout(10, TimeUnit.SECONDS);
		// 是否自动重连
		newBuilder.retryOnConnectionFailure(true).connectionPool(new ConnectionPool(10, 5L, TimeUnit.MINUTES));
		return newBuilder.build();

	}

	@Bean
	public Retryer feignRetryer() {
		return new Retryer.Default(100, SECONDS.toMillis(1),3);
	}
}
