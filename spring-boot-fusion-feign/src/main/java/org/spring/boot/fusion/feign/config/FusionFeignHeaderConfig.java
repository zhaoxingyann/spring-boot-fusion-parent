package org.spring.boot.fusion.feign.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class FusionFeignHeaderConfig implements RequestInterceptor {

	@Override
	public void apply(RequestTemplate requestTemplate) {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes != null) {
			ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
			if (attributes != null) {
				HttpServletRequest request = attributes.getRequest();
				requestTemplate.header("Authorization", request.getHeader("Authorization"));
			}
		}
	}

}
