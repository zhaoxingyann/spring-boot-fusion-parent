package org.spring.boot.fusion.feign.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class FusionFeignLogConfig {

	@Bean
	Logger.Level feignLogger() {
		return Logger.Level.FULL;
	}

}
