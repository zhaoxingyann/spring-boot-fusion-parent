package org.spring.boot.fusion.auth.sso.service.impl;

import java.util.Map;

import org.spring.boot.fusion.auth.sso.service.FusionLoginCallbackService;

public class FusionLoginCallbackServiceImpl implements FusionLoginCallbackService {

	@Override
	public Map<String,Object> loginCallback(Map<String,Object> hashMap) {
		return hashMap;
	}

}
