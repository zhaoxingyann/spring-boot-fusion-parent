package org.spring.boot.fusion.auth.sso.service;

import javax.servlet.http.HttpServletRequest;

public interface FusionPermissionsService {

	public boolean checkHavePermissions(HttpServletRequest request);

}
