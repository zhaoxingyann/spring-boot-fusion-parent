package org.spring.boot.fusion.auth.sso.config;

import org.spring.boot.fusion.auth.sso.service.FusionLoginCallbackService;
import org.spring.boot.fusion.auth.sso.service.impl.FusionLoginCallbackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FusionLoginCallbackConfig {

	@Autowired(required = false)
	private FusionLoginCallbackService loginCallbackService;
	
	@Bean
	public FusionLoginCallbackService loginCallbackService() {
		if (loginCallbackService == null) {
			loginCallbackService = new FusionLoginCallbackServiceImpl();
		}
		return loginCallbackService;
	}
}
