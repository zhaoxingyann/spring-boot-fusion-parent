package org.spring.boot.fusion.auth.sso.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.spring.boot.fusion.common.constant.ConstantAttribute;
import org.spring.boot.fusion.common.exception.ServiceErrorStatus;
import org.spring.boot.fusion.common.response.ResponseData;
import org.spring.boot.fusion.common.tools.JsonTool;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class FusionLoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		response.setHeader("Access-Control-Allow-Origin", "*");		
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(ConstantAttribute.JSON_TYPE_UTF8);
		if(exception instanceof BadCredentialsException){
			response.getWriter().write(JsonTool.ObjectToJson(ResponseData.ERROR(ServiceErrorStatus.PASSWORD_ERROR)));
		}else if(exception instanceof InternalAuthenticationServiceException){
			response.getWriter().write(JsonTool.ObjectToJson(ResponseData.ERROR(ServiceErrorStatus.NOT_USER)));
		}else{
			response.getWriter().write(JsonTool.ObjectToJson(ResponseData.ERROR(ServiceErrorStatus.AUTHENTICATION_FAILED)));
		}
	}
}
