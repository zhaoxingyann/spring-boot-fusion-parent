package org.spring.boot.fusion.format;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FormatClassBean {

	private static ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

	public <T> T formatClass(Class<T> clazz) {
		return jsonToClass(ObjectToJson(this), clazz);
	}

	public String formatString() {
		return ObjectToJson(this);
	}

	private static String ObjectToJson(Object object) {
		String writeValueAsString = null;
		try {
			writeValueAsString = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return writeValueAsString;
	}

	private static <T> T jsonToClass(String jsonString, Class<T> clazz) {
		T readValue = null;
		try {
			readValue = mapper.readValue(jsonString, clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readValue;
	}
}
