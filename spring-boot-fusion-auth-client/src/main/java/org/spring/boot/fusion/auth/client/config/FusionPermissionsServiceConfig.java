package org.spring.boot.fusion.auth.client.config;

import org.spring.boot.fusion.auth.client.service.FusionPermissionsService;
import org.spring.boot.fusion.auth.client.service.impl.FusionPermissionsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class FusionPermissionsServiceConfig {

	@Autowired(required = false)
	private FusionPermissionsService fusionPermissionsService;

	@Bean
	public FusionPermissionsService permissionsService() {
		if (fusionPermissionsService == null) {
			fusionPermissionsService = new FusionPermissionsServiceImpl();
		}
		return fusionPermissionsService;
	}
}
