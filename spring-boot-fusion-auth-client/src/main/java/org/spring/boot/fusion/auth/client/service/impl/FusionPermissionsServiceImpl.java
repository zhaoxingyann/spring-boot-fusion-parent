package org.spring.boot.fusion.auth.client.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.spring.boot.fusion.auth.client.service.FusionPermissionsService;
import org.spring.boot.fusion.security.user.FusionUserData;

public class FusionPermissionsServiceImpl implements FusionPermissionsService {

	@Override
	public boolean checkHavePermissions(HttpServletRequest request) {
		String requestURI = request.getRequestURI();
		List<String> permissions = FusionUserData.getUserData().getPermissions();
		if (permissions != null && permissions.size() > 0 && requestURI != null && requestURI.length() > 0
				&& permissions.contains(requestURI)) {
			return true;
		} else {
			return false;
		}
	}

}
