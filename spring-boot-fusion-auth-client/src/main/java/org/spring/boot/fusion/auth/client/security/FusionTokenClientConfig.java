package org.spring.boot.fusion.auth.client.security;

import java.util.List;

import org.spring.boot.fusion.auth.client.filter.FusionPermissionsValidateFilter;
import org.spring.boot.fusion.auth.client.service.FusionPermissionsService;
import org.spring.boot.fusion.common.constant.ConstantAttribute;
import org.spring.boot.fusion.common.tools.ConcurrentTools;
import org.spring.boot.fusion.security.properties.FusionTokenProperties;
import org.spring.boot.fusion.security.tools.NotLoginAuthUrlTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableResourceServer
public class FusionTokenClientConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private RedisConnectionFactory redisConnectionFactory;
	@Autowired
	private FusionTokenProperties tokenProperties;
	@Autowired
	private FusionPermissionsService permissionsService;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		//权限拦截
		FusionPermissionsValidateFilter permissionsValidateFilter = new FusionPermissionsValidateFilter();
		permissionsValidateFilter.setTokenProperties(tokenProperties);
		permissionsValidateFilter.setPermissionsService(permissionsService);
		http.addFilterAfter(permissionsValidateFilter, UsernamePasswordAuthenticationFilter.class);
		// 设置请求拦截
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http
				.authorizeRequests();
		String[] noRelease = tokenProperties.getNoRelease();
		if (noRelease != null && noRelease.length > 0) {
			for (String noReleaseUrl : noRelease) {
				if (noReleaseUrl != null && !noReleaseUrl.equalsIgnoreCase(ConstantAttribute.NULL) && noReleaseUrl.length() > 0) {
					authorizeRequests.antMatchers(noReleaseUrl).permitAll();
				}
			}
		}
		List<String> notAuthUrlList = NotLoginAuthUrlTools.getNotAuthUrlList(tokenProperties.getMethodsPackage());
		if(notAuthUrlList!=null && notAuthUrlList.size() > 0) {
			for (String notAuthUrl : notAuthUrlList) {
				authorizeRequests.antMatchers(notAuthUrl).permitAll();
			}
			ConcurrentTools.getConcurrentHashMap().put(ConstantAttribute.NOT_AUTH_URL_LIST,notAuthUrlList);
		}
		authorizeRequests.antMatchers(HttpMethod.OPTIONS).permitAll();
		// 设置拦截所有的请求
		authorizeRequests.anyRequest().authenticated().and().csrf().disable();
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenStore(redisTokenStore());
	}

	@Bean
	public TokenStore redisTokenStore() {
		return new RedisTokenStore(redisConnectionFactory);
	}

}
