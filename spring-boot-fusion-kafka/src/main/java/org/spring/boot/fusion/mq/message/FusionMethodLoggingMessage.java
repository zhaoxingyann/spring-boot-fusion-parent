package org.spring.boot.fusion.mq.message;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.spring.boot.fusion.common.tools.JsonTool;
import org.spring.boot.fusion.mq.kafka.KafkaAcceptLoggingWriteMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class FusionMethodLoggingMessage {

	@Autowired
	private KafkaAcceptLoggingWriteMessage acceptLogsWriteService;

	@Async
	public void sendBusinessLogs(HttpServletRequest request, JoinPoint joinPoint) throws Exception {
		acceptLogsWriteService.sendMessage("日志：" + UUID.randomUUID().toString());
	}

	@Async
	public void sendBusinessLogs(Object object) throws Exception {
		acceptLogsWriteService.sendMessage(JsonTool.ObjectToJson(object));
	}

	@Async
	public void sendBusinessLogs(Map<String, Object> map) throws Exception {
		acceptLogsWriteService.sendMessage(JsonTool.ObjectToJson(map));
	}
}