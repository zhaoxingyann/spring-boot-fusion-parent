package org.spring.boot.fusion.mq.aop;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.spring.boot.fusion.mq.message.FusionMethodLoggingMessage;
import org.spring.boot.fusion.mq.properties.FusionLoggingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class FusionMethodLoggingAop {

	@Autowired
	private FusionLoggingProperties fusionLogsProperties;
	
	@Autowired
	private FusionMethodLoggingMessage fusionMethodLoggingMessage;

	@Pointcut(value = "within(@org.springframework.web.bind.annotation.RestController *)")
	private void anyMethod() {

	}

	@Before(value = "anyMethod()")
	public void doBefor(JoinPoint joinPoint) throws Exception {
		if(fusionLogsProperties.getBusinessLog()) {
			ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = attributes.getRequest();
			fusionMethodLoggingMessage.sendBusinessLogs(request,joinPoint);
		}
	}
}