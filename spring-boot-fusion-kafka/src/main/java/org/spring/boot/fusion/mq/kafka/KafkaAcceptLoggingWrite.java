package org.spring.boot.fusion.mq.kafka;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface KafkaAcceptLoggingWrite {

	String OUTPUT = "logsWrite";

	@Output(KafkaAcceptLoggingWrite.OUTPUT)
	MessageChannel output();

}