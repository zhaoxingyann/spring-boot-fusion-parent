package org.spring.boot.fusion.mq.properties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("spring.fusion.logs")
public class FusionLoggingProperties {

	private Boolean businessLog=false;

	public Boolean getBusinessLog() {
		return businessLog;
	}

	public void setBusinessLog(Boolean businessLog) {
		this.businessLog = businessLog;
	}
}