package org.spring.boot.fusion.mq.kafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

@EnableBinding(KafkaAcceptLoggingWrite.class)
public class KafkaAcceptLoggingWriteMessage {

	@Autowired
	private KafkaAcceptLoggingWrite acceptLogsWrite;

	public void sendMessage(String message) {
		try {
			acceptLogsWrite.output().send(MessageBuilder.withPayload(message).build());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}