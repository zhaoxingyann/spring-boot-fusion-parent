package org.spring.boot.fusion.security.tools;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.reflections.Reflections;
import org.spring.boot.fusion.common.annotation.NotLoginAuth;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

public class NotLoginAuthUrlTools {

	public static List<String> getNotAuthUrlList(String methodsPackage) {
		List<String> arrayList = new ArrayList<>();
		Set<String> notAuthUrlSets = getNotAuthUrlSets(methodsPackage);
		if (notAuthUrlSets != null && notAuthUrlSets.size() > 0) {
			for (String url : notAuthUrlSets) {
				arrayList.add(url);
			}
		}
		return arrayList;
	}

	private static Set<String> getNotAuthUrlSets(String methodsPackage) {
		if (methodsPackage == null || methodsPackage.trim().length() == 0) {
			return null;
		}
		Set<String> hashSet = new HashSet<>();
		Reflections reflections = new Reflections(methodsPackage);
		Set<Class<?>> classList = reflections.getTypesAnnotatedWith(RestController.class);
		for (Class<?> classes : classList) {
			String pathValue = "";
			RequestMapping requestMappingClass = classes.getAnnotation(RequestMapping.class);
			NotLoginAuth notLoginAuthClass = classes.getAnnotation(NotLoginAuth.class);
			if (requestMappingClass != null) {
				String[] value = requestMappingClass.value();
				if (value != null && value.length > 0) {
					String path = value[0];
					if (path != null && path.length() > 0 && !pathValue.equals("/")) {
						pathValue = pathValue + path;
					}
				}

			}
			Method[] methods = classes.getMethods();
			if (methods != null && methods.length > 0) {
				for (Method method : methods) {
					String[] values = null;
					PostMapping postMapping = method.getAnnotation(PostMapping.class);
					if (postMapping != null) {
						values = postMapping.value();
					}
					GetMapping getMapping = method.getAnnotation(GetMapping.class);
					if (getMapping != null) {
						values = getMapping.value();
					}
					RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
					if (requestMapping != null) {
						values = requestMapping.value();
					}
					if (notLoginAuthClass == null) {
						NotLoginAuth notLoginAuth = method.getAnnotation(NotLoginAuth.class);
						if (notLoginAuth == null) {
							values = null;
						}
					}
					if (values != null && values.length > 0) {
						for (String value : values) {
							if (value != null && value.trim().length() > 0) {
								String url = "";
								if (value.startsWith("/")) {
									url = pathValue + value;
								} else {
									url = pathValue + "/" + value;
								}
								if (!url.startsWith("/")) {
									url = "/" + url;
								}
								hashSet.add(url);
							}
						}
					}
				}
			}
		}
		return hashSet;
	}
}