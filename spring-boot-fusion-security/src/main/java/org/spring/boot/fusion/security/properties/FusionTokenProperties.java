package org.spring.boot.fusion.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("spring.fusion.token")
public class FusionTokenProperties {
	
	/**
	 * token账号
	 */
	private String username="client_id_name";
	
	/**
	 * taoken密码
	 */
	private String password="client_id_pass";
	
	/**
	 * token的有效时间
	 */
    private Integer accessTokenTime=6000;
	
    /**
         * 刷新token的有效时间
     */
	private Integer refreshTokenTime=9000000;

	/**
	 * 登陆的地址
	 */
	private String userLoginUrl = "/user/login";

	/**
	 * 退出的地址
	 */
	private String tokenLogoutUrl = "/loginExit";

	/**
	 * 异常的地址
	 */
	private String loginErrorUrl = "/loginError";
	
	/**
	 * 刷新的地址
	 */
	private String refreshTokenUrl="/loginRefresh";
	
	/**
	 * 是否限制一个用户登陆
	 */
	private boolean loginNumberOnly=false;

	/**
	 * 是否开启权限认证
	 */
	private boolean permissionsAuth=false;

	/**
	 * 不需要认证的的地址
	 */
	private String[] noRelease;
	
	/**
	 * 限制其他接口的使用
	 */
	private boolean checkUrl=true;
	
	/**
	 * 方法包
	 */
	private String methodsPackage;

}
