package org.spring.boot.fusion.security.user;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.spring.boot.fusion.common.tools.ThreadLocalTool;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class FusionUserDetails implements UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String username;
	private String password;
	private boolean enabled=true;
	private boolean accountNonExpired=true;
	private boolean accountNonLocked=true;
	private boolean credentialsNonExpired=true;
	@JsonIgnore
	private List<String> permissions;
	private Collection<? extends GrantedAuthority> authorities=AuthorityUtils.commaSeparatedStringToAuthorityList("");
	//需要返回的值
	private Map<String,Object> responseMap=new HashMap<String,Object>();
	//不需要返回的值
	private Map<String,Object> dataMap=new HashMap<String,Object>();
	
	public FusionUserDetails(String username,String password) {
		this.username=username;
		this.password=password;
		ThreadLocalTool.set(true);
	}
	
	public FusionUserDetails(String username,String password,boolean checkPassword) {
		this.username=username;
		this.password=password;
		ThreadLocalTool.set(checkPassword);
	}

	public Map<String, Object> getDataMap() {
		dataMap.putAll(this.getResponseMap());
		return dataMap;
	}
	
}
