package org.spring.boot.fusion.security.password;

import org.spring.boot.fusion.common.tools.ThreadLocalTool;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class FusionPasswordEncoder implements PasswordEncoder {

	private PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	@Override
	public String encode(CharSequence rawPassword) {
		return bCryptPasswordEncoder.encode(rawPassword);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		Object object = ThreadLocalTool.get();
		if (object != null && !Boolean.valueOf(object.toString())) {
			return true;
		} else {
			return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
		}

	}
}
