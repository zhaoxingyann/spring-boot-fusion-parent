package org.spring.boot.fusion.security.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class FusionUserData {
	public static final String ANONY_MOUS_USER = "anonymousUser";

	public static FusionUserDetails getUserData() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		Object principal = authentication.getPrincipal();
		if (principal == null || ANONY_MOUS_USER.equalsIgnoreCase(principal.toString())) {
			return null;
		}
		FusionUserDetails userData=(FusionUserDetails) principal;
		return userData;
	}

}
