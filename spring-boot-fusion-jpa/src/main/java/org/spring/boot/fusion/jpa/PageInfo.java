package org.spring.boot.fusion.jpa;

public class PageInfo {

	private Integer current;

	private Integer size;

	public PageInfo(Integer pageIndex, Integer pageSize) {
		if (pageSize == null || pageSize < 0) {
			this.size = 10;
		} else {
			this.size = pageSize;
		}
		if (pageIndex == null || pageIndex < 0 || pageIndex == 0) {
			this.current = 0;
		} else {
			this.current = (pageIndex - 1) * this.size;
		}
	}

	public Integer getCurrent() {
		return current;
	}

	public Integer getSize() {
		return size;
	}
}
