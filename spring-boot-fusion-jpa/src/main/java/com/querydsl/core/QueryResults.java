package com.querydsl.core;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;

public final class QueryResults<T> implements Serializable {
	private static final long serialVersionUID = -4591506147471300909L;

	private static final QueryResults<Object> EMPTY = new QueryResults<Object>(ImmutableList.of(), Long.MAX_VALUE, 0L,
			0L);

	@SuppressWarnings("unchecked")
	public static <T> QueryResults<T> emptyResults() {
		return (QueryResults<T>) EMPTY;
	};

	private final long size, current, pages, total;

	private final List<T> results;
	public QueryResults(List<T> results, @Nullable Long limit, @Nullable Long offset, long total) {
		this.size = limit != null ? limit : Long.MAX_VALUE;
		this.total = total;
		this.results = results;
		Long pageIndex = (total - 1) / limit + 1;
		Long currentIndex = offset / limit + 1;
		this.current = currentIndex;
		this.pages = pageIndex;
	}

	public QueryResults(List<T> results, QueryModifiers mod, long total) {
		this(results, mod.getLimit(), mod.getOffset(), total);
	}

	public List<T> getResults() {
		return results;
	}

	public long getTotal() {
		return total;
	}

	public boolean isEmpty() {
		return results.isEmpty();
	}

	public long getSize() {
		return size;
	}

	public long getCurrent() {
		return current;
	}

	public long getPages() {
		return pages;
	}

}
