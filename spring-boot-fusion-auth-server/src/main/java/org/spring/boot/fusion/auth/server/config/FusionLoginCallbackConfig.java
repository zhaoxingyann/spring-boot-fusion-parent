package org.spring.boot.fusion.auth.server.config;

import org.spring.boot.fusion.auth.server.service.FusionLoginCallbackService;
import org.spring.boot.fusion.auth.server.service.impl.FusionLoginCallbackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FusionLoginCallbackConfig {

	@Autowired(required = false)
	private FusionLoginCallbackService loginCallbackService;
	
	@Bean
	public FusionLoginCallbackService loginCallbackService() {
		if (loginCallbackService == null) {
			loginCallbackService = new FusionLoginCallbackServiceImpl();
		}
		return loginCallbackService;
	}
}
