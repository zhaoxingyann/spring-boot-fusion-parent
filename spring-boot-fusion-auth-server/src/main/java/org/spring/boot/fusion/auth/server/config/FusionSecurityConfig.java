package org.spring.boot.fusion.auth.server.config;

import org.spring.boot.fusion.auth.server.filter.FusionLimitAddressFilter;
import org.spring.boot.fusion.auth.server.filter.FusionOnlyUserFilter;
import org.spring.boot.fusion.auth.server.handler.FusionLoginFailureHandler;
import org.spring.boot.fusion.auth.server.handler.FusionLoginSuccessHandler;
import org.spring.boot.fusion.security.password.FusionPasswordEncoder;
import org.spring.boot.fusion.security.properties.FusionTokenProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
@Configuration
public class FusionSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private TokenStore tokenStore;
	@Autowired
	private FusionLoginSuccessHandler loginSuccessHandler;
	@Autowired
	private FusionLoginFailureHandler loginFailureHandler;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private FusionTokenProperties tokenConfigProperties;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//限制使用接口的拦截器
		FusionLimitAddressFilter fusionLimitAddressFilter = new FusionLimitAddressFilter();
		fusionLimitAddressFilter.setFusionTokenProperties(tokenConfigProperties);
		//限制一个用户登陆
		FusionOnlyUserFilter fusionOnlyUserFilter = new FusionOnlyUserFilter();
		fusionOnlyUserFilter.setTokenStore(tokenStore);
		fusionOnlyUserFilter.setTokenConfigProperties(tokenConfigProperties);
		
		http
		//在认证之前验证接口是否可以访问
		.addFilterBefore(fusionLimitAddressFilter, UsernamePasswordAuthenticationFilter.class)
		//限制一个用户登陆
		.addFilterAfter(fusionOnlyUserFilter, FusionLimitAddressFilter.class)
		// 设置表单登陆
		.formLogin()
		// 设置登录页（rest风格设置错误的登录页）
		.loginPage(tokenConfigProperties.getLoginErrorUrl())
		// 登陆的方法
		.loginProcessingUrl(tokenConfigProperties.getUserLoginUrl())
		// 登陆成功之后需要处理的方法
		.successHandler(loginSuccessHandler)
		// 登陆失败之后需要处理的方法
		.failureHandler(loginFailureHandler)
		//登陆的实现方法
		.and().userDetailsService(userDetailsService)
		// 设置请求拦截
		.authorizeRequests()
		// 不需要拦截的方法
		.antMatchers(tokenConfigProperties.getUserLoginUrl()).permitAll()
		.antMatchers(tokenConfigProperties.getLoginErrorUrl()).permitAll()
		.antMatchers(tokenConfigProperties.getTokenLogoutUrl()).permitAll()
		.antMatchers(tokenConfigProperties.getRefreshTokenUrl()).permitAll()
		.antMatchers(HttpMethod.OPTIONS).permitAll()
		// 设置拦截所有的请求
		.anyRequest().authenticated()
		// 关闭跨站的问题
		.and().csrf().disable();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new FusionPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
}
