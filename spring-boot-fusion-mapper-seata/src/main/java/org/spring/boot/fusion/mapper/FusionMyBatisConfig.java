package org.spring.boot.fusion.mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.config.GlobalConfig.DbConfig;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

/**
 * @Description: 如果在配置文件里面配置这些信息，这个类可以删除掉，建议使用代码配置，因为这些配置不会轻易的改变，可以理解为不会改变
 * @author: 赵兴炎
 * @date: 2019年7月9日
 */
@Configuration
public class FusionMyBatisConfig {

	@Autowired(required = false)
	private MybatisPlusProperties mybatisPlusProperties;

	/**
	 * @Description: 属性配置
	 * @author: 赵兴炎
	 * @date: 2019年7月15日
	 * @return: 返回值
	 */
	@Bean @Primary
	@ConfigurationProperties(prefix = Constants.MYBATIS_PLUS)
	public MybatisPlusProperties mybatisPlusProperties() {
		if (mybatisPlusProperties == null) {
			mybatisPlusProperties = new MybatisPlusProperties();
		}
		DbConfig dbConfig = new DbConfig();
		// 分布式id
//		dbConfig.setIdType(IdType.ID_WORKER_STR);
		// 开启大写命名
		dbConfig.setCapitalMode(true);
		mybatisPlusProperties.setGlobalConfig(new GlobalConfig().setDbConfig(dbConfig));
		MybatisConfiguration configuration = new MybatisConfiguration();
		// 驼峰命名
		configuration.setMapUnderscoreToCamelCase(true);
		// 对空值的处理
		configuration.setCallSettersOnNulls(true);
		mybatisPlusProperties.setConfiguration(configuration);
		return mybatisPlusProperties;
	}
	
	/**
	 * @Description: 分页配置
	 * @author: 赵兴炎
	 * @date: 2019年7月15日
	 * @return: 返回值
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
}