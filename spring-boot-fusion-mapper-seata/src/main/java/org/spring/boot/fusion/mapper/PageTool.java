package org.spring.boot.fusion.mapper;

import java.util.List;

import org.spring.boot.fusion.common.tools.JsonTool;

import com.baomidou.mybatisplus.core.metadata.IPage;

@SuppressWarnings("all")
public class PageTool {

	public static <T> IPage<T> toClass(IPage page, Class<T> clazz) {
		List records = page.getRecords();
		List<T> listToList = JsonTool.ListToList(records, clazz);
		IPage<T> setRecords = page.setRecords(listToList);
		return setRecords;
	}
}
