package org.spring.boot.fusion.auth.server.database;

import org.spring.boot.fusion.common.exception.ServiceErrorStatus;
import org.spring.boot.fusion.common.exception.ServiceRunTimeException;
import org.spring.boot.fusion.common.tools.SpringBeanTool;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;

public class DatabaseClientDetailsService implements ClientDetailsService {

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		DatabaseClientDetailsBean databaseClientDetailsBean = null;
		FusionClientDetailsService fusionClientDetailsService = SpringBeanTool.getBean(FusionClientDetailsService.class);
		if (fusionClientDetailsService != null) {
			databaseClientDetailsBean = fusionClientDetailsService.getDatabaseClientDetailsBean(clientId);
		}
		if (databaseClientDetailsBean != null) {
			return new DatabaseClientDetails(databaseClientDetailsBean);
		} else {
			throw new ServiceRunTimeException(ServiceErrorStatus.AUTHENTICATION_FAILED);
		}
	}

}
