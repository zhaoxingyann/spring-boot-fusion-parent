package org.spring.boot.fusion.auth.server.database;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

public class DatabaseClientDetails implements ClientDetails {

	private static final long serialVersionUID = 1L;

	private DatabaseClientDetailsBean databaseClientDetailsBean;

	public DatabaseClientDetails(DatabaseClientDetailsBean databaseClientDetailsBean) {
		this.databaseClientDetailsBean = databaseClientDetailsBean;
	}

	@Override
	public String getClientId() {
		return databaseClientDetailsBean.getUserName();
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return databaseClientDetailsBean.getAccessTokenValiditySeconds();
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return databaseClientDetailsBean.getRefreshTokenValiditySeconds();
	}

	@Override
	public String getClientSecret() {
		return databaseClientDetailsBean.getPaasWord();
	}

	@Override
	public Set<String> getScope() {
		return databaseClientDetailsBean.getScope();
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return databaseClientDetailsBean.getAuthorizedGrantTypes();
	}

	@Override
	public Set<String> getResourceIds() {
		return null;
	}

	@Override
	public boolean isSecretRequired() {
		return false;
	}

	@Override
	public boolean isScoped() {
		return false;
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		return null;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public boolean isAutoApprove(String scope) {
		return true;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		return null;
	}

}
