package org.spring.boot.fusion.auth.server.business.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.spring.boot.fusion.auth.server.business.service.FusionTokenCrudService;
import org.spring.boot.fusion.common.constant.ConstantAttribute;
import org.spring.boot.fusion.common.exception.ServiceErrorStatus;
import org.spring.boot.fusion.common.exception.ServiceRunTimeException;
import org.spring.boot.fusion.common.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FusionTokenCrudController {

	@Autowired
	private FusionTokenCrudService tokenCrudService;
	
	@PostMapping("loginRefresh")
	public ResponseEntity<ResponseData> refreshToken(String refresh_token) throws Exception {
		Map<String,Object> tokeMap=tokenCrudService.refreshToken(refresh_token);
		return ResponseEntity.ok(ResponseData.SUCCESS(tokeMap));
	}
	
	@GetMapping("loginError")
	public ResponseEntity<ResponseData> loginError(HttpServletRequest request) {
		return ResponseEntity.ok(ResponseData.ERROR(ServiceErrorStatus.AUTHENTICATION_FAILED));
	}
	
	@PostMapping("loginExit")
	public ResponseEntity<ResponseData> tokenLogout(HttpServletRequest request) {
		String deleteToken = tokenCrudService.deleteToken(request);
		if(deleteToken!=null && deleteToken.equalsIgnoreCase(ConstantAttribute.SUCCESS_SATE)) {
			return ResponseEntity.ok(ResponseData.SUCCESS());
		}else {
			throw new ServiceRunTimeException(ServiceErrorStatus.LOGIN_EXIT);
		}
	}
}
