package org.spring.boot.fusion.auth.server.database;

public interface FusionClientDetailsService {

	DatabaseClientDetailsBean getDatabaseClientDetailsBean(String clientId);
}
