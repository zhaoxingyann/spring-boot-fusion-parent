package org.spring.boot.fusion.auth.server.service;

import java.util.Map;

public interface FusionLoginCallbackService {
	
	public Map<String,Object> loginCallback(Map<String,Object> hashMap);
}
