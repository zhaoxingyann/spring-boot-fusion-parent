package org.spring.boot.fusion.auth.server.database;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DatabaseClientDetailsBean {

	public String userName;

	public String paasWord;

	public Set<String> scope = new HashSet<>();

	public Set<String> authorizedGrantTypes = new HashSet<>();

	public Integer accessTokenValiditySeconds;

	public Integer refreshTokenValiditySeconds;

	public Set<String> getScope() {
		if (scope == null || scope.size() == 0) {
			scope.add("all");
		}
		return scope;
	}

	public Set<String> getAuthorizedGrantTypes() {
		if (authorizedGrantTypes == null || authorizedGrantTypes.size() == 0) {
			authorizedGrantTypes.add("authorization_code");
			authorizedGrantTypes.add("client_credentials");
			authorizedGrantTypes.add("refresh_token");
			authorizedGrantTypes.add("password");
			authorizedGrantTypes.add("implicit");
		}
		return authorizedGrantTypes;
	}

	public Integer getAccessTokenValiditySeconds() {
		if (accessTokenValiditySeconds == null) {
			accessTokenValiditySeconds = 60000;
		}
		return accessTokenValiditySeconds;
	}

	public Integer getRefreshTokenValiditySeconds() {
		if (refreshTokenValiditySeconds == null) {
			refreshTokenValiditySeconds = 600000;
		}
		return refreshTokenValiditySeconds;
	}

}
