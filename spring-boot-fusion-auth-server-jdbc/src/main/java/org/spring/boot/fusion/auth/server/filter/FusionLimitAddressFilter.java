package org.spring.boot.fusion.auth.server.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.spring.boot.fusion.security.properties.FusionTokenProperties;
import org.spring.boot.fusion.security.tools.FusionCheckUrlTools;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
  * 限制其他接口的使用
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FusionLimitAddressFilter extends OncePerRequestFilter {

	private FusionTokenProperties fusionTokenProperties;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if(fusionTokenProperties.isCheckUrl()) {
			if(FusionCheckUrlTools.checkUrl(request.getRequestURI(), fusionTokenProperties,request)) {
				filterChain.doFilter(request, response);
			}else{
				response.setStatus(HttpStatus.NOT_FOUND.value());
			}
		}else {
			filterChain.doFilter(request, response);
		}
	}

}
