package org.spring.boot.fusion.auth.server.business.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface FusionTokenCrudService {
	
	String deleteToken(HttpServletRequest request);

	Map<String, Object> refreshToken(String refresh_token);
}
