package org.spring.boot.fusion.common.params;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RequestParams<T> {

    public T params;
    
    public int pageSize;
    
    public int pageIndex;
    
    public RequestParams() {}
    
    public RequestParams(T params) {
    	this.params=params;
    }
    
    public RequestParams(int pageIndex,int pageSize) {
    	this.pageIndex=pageIndex;
    	this.pageSize=pageSize;
    }
    
    public RequestParams(T params,int pageIndex,int pageSize) {
    	this.params=params;
    	this.pageIndex=pageIndex;
    	this.pageSize=pageSize;
    }
    
}
