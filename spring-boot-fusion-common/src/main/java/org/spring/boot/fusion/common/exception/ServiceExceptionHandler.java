package org.spring.boot.fusion.common.exception;
import javax.servlet.http.HttpServletResponse;
import org.spring.boot.fusion.common.response.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ServiceExceptionHandler {

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ResponseData> defaultErrorHandler(HttpServletResponse response, Exception exception) {
		exception.printStackTrace();
		if (exception instanceof ServiceRunTimeException) {
			ServiceRunTimeException httpException = (ServiceRunTimeException) exception;
			Integer status = httpException.getStatus();
			String message = httpException.getMessage();
			String describe = httpException.getDescribe();
			ResponseData errorMessage = ResponseData.ERROR(status, message, describe);
			return ResponseEntity.status(httpException.getHttpStatus()).body(errorMessage);
		}
		int status = HttpStatus.BAD_REQUEST.value();
		String message = "请求异常";
		String describe = exception.getMessage();
		ResponseData errorMessage = ResponseData.ERROR(status, message, describe);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
	}
}
