package org.spring.boot.fusion.common.exception;

import org.springframework.http.HttpStatus;

public class ServiceRunTimeException extends RuntimeException {
	private static final long serialVersionUID = -2319097667755215294L;
	
	private Integer status;
	private String message;
	private String describe;
	private HttpStatus httpStatus;

	public ServiceRunTimeException() {
		super();
	}
	
	public ServiceRunTimeException(String message) {
		this.message=message;
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(String message,String describe) {
		this.message=message;
		this.describe=describe;
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(ServiceErrorStatus errorStatus) {
		this.status=errorStatus.status();
		this.message=errorStatus.message();
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(ServiceErrorStatus errorStatus,String describe) {
		this.status=errorStatus.status();
		this.message=errorStatus.message();
		this.describe=describe;
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(Integer status,String message) {
		this.status=status;
		this.message=message;
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(Integer status,String message,String describe) {
		this.status=status;
		this.message=message;
		this.describe=describe;
		this.httpStatus=HttpStatus.BAD_REQUEST;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,String message) {
		this.message=message;
		this.httpStatus=httpStatus;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,String message,String describe) {
		this.message=message;
		this.describe=describe;
		this.httpStatus=httpStatus;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,ServiceErrorStatus errorStatus) {
		this.status=errorStatus.status();
		this.message=errorStatus.message();
		this.httpStatus=httpStatus;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,ServiceErrorStatus errorStatus,String describe) {
		this.status=errorStatus.status();
		this.message=errorStatus.message();
		this.describe=describe;
		this.httpStatus=httpStatus;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,Integer status,String message) {
		this.status=status;
		this.message=message;
		this.httpStatus=httpStatus;
	}
	
	public ServiceRunTimeException(HttpStatus httpStatus,Integer status,String message,String describe) {
		this.status=status;
		this.message=message;
		this.describe=describe;
		this.httpStatus=httpStatus;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

}
