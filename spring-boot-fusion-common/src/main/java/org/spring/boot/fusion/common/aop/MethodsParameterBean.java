package org.spring.boot.fusion.common.aop;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MethodsParameterBean {

	private String methodsType;
	
	private String url;
	
	private String uri;
	
	private Object params;
	
	private String serverName;
	
	private String contentType;
	
	private String methodsDescribe;
	
	private Boolean authFlag=false;
}
