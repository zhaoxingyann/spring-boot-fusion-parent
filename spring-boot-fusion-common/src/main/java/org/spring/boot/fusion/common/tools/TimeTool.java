package org.spring.boot.fusion.common.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.spring.boot.fusion.common.constant.TimeEnum;

public class TimeTool {

	/**
	 * @Description: 字符串转时间最小值
	 * @param 时间字符串 yyyy-MM-dd
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static Date minStringDate(String timeString) {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeEnum.YMDHMS.value());
		try {
			timeString = timeString + " 00:00:00.000";
			date = simpleDateFormat.parse(timeString);
		} catch (Exception e) {
			throw new RuntimeException("字符串转时间最小值异常");
		}
		return date;
	}

	/**
	 * @Description: 字符串转时间最大值
	 * @param 时间字符串 yyyy-MM-dd
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static Date maxStringDate(String timeString) {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeEnum.YMDHMS.value());
		try {
			timeString = timeString + " 23:59:59.000";
			date = simpleDateFormat.parse(timeString);
		} catch (Exception e) {
			throw new RuntimeException("字符串转时间最大值异常");
		}
		return date;
	}
	
	/**
	 * @Description: 字符串转时间
	 * @param 时间字符串 yyyy-MM-dd
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static Date stringToDate(String timeString) {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeEnum.YMDHMS.value());
		try {
			date = simpleDateFormat.parse(timeString);
		} catch (Exception e) {
			throw new RuntimeException("字符串转时间异常");
		}
		return date;
	}
}
