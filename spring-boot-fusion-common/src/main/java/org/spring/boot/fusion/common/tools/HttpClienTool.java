package org.spring.boot.fusion.common.tools;

import java.util.Map;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HttpClienTool {

	private static ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

	private static RestTemplate restTemplate = new RestTemplate();

	/**
	 * @Description:请求调用方法
	 * @param parameter  参数
	 * @param url        请求地址
	 * @param httpMethod 请求方法
	 * @param clazz      返回值类型
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static <T> T exchange(Object parameter, String url, HttpMethod httpMethod, Class<T> clazz) {
		ResponseEntity<T> exchange = null;
		try {
			HttpEntity<String> httpEntity = getHttpEntity(parameter, null);
			exchange = restTemplate.exchange(url, httpMethod, httpEntity, clazz);
		} catch (HttpClientErrorException e) {
			System.err.println(e.getResponseBodyAsString());
		}
		return exchange.getBody();
	}

	/**
	 * @Description:请求调用方法
	 * @param parameter  参数
	 * @param url        请求地址
	 * @param httpMethod 请求方法
	 * @param clazz      返回值类型
	 * @param headers    请求header数据
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static <T> T exchange(Object parameter, String url, HttpMethod httpMethod, Class<T> clazz,
			Map<String, String> headers) {
		ResponseEntity<T> exchange = null;
		try {
			HttpEntity<String> httpEntity = getHttpEntity(parameter, headers);
			exchange = restTemplate.exchange(url, httpMethod, httpEntity, clazz);
		} catch (HttpClientErrorException e) {
			System.err.println(e.getResponseBodyAsString());
		}
		return exchange.getBody();
	}

	/**
	 * @Description:请求调用方法
	 * @param url        请求地址
	 * @param httpMethod 请求方法
	 * @param clazz      返回值类型
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static <T> T exchange(String url, HttpMethod httpMethod, Class<T> clazz) {
		ResponseEntity<T> exchange = null;
		try {
			HttpEntity<String> httpEntity = getHttpEntity(null, null);
			exchange = restTemplate.exchange(url, httpMethod, httpEntity, clazz);
		} catch (HttpClientErrorException e) {
			System.err.println(e.getResponseBodyAsString());
		}
		return exchange.getBody();
	}

	/**
	 * @Description:请求调用方法
	 * @param url        请求地址
	 * @param httpMethod 请求方法
	 * @param clazz      返回值类型
	 * @param headers    请求header数据
	 * @author: 赵兴炎
	 * @date: 2019年7月10日
	 * @return: 返回值
	 */
	public static <T> T exchange(String url, HttpMethod httpMethod, Class<T> clazz, Map<String, String> headers) {
		ResponseEntity<T> exchange = null;
		try {
			HttpEntity<String> httpEntity = getHttpEntity(null, headers);
			exchange = restTemplate.exchange(url, httpMethod, httpEntity, clazz);
		} catch (HttpClientErrorException e) {
			System.err.println(e.getResponseBodyAsString());
		}
		return exchange.getBody();
	}

	private static HttpEntity<String> getHttpEntity(Object parameter, Map<String, String> headers) {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.parseMediaType("application/json; charset=UTF-8"));
		if (headers != null && headers.size() > 0) {
			for (String key : headers.keySet()) {
				requestHeaders.add(key, headers.get(key));
			}
		}
		return new HttpEntity<>(ObjectToJson(parameter), requestHeaders);
	}

	private static String ObjectToJson(Object object) {
		String writeValueAsString = null;
		try {
			writeValueAsString = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return writeValueAsString;
	}

}
