package org.spring.boot.fusion.common.tools;

import java.util.concurrent.ConcurrentHashMap;


public class ConcurrentTools {

	private static ConcurrentHashMap<String, Object> concurrentHashMap = new ConcurrentHashMap<>();

	public static ConcurrentHashMap<String, Object> getConcurrentHashMap() {
		return concurrentHashMap;
	}
}
