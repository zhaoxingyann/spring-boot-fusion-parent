package org.spring.boot.fusion.common.constant;

public interface ConstantAttribute {

	// 自定义标识字段--失败标示
	public static final String FAILURE_SATE = "0";
	// 自定义标识字段--成功标示
	public static final String SUCCESS_SATE = "1";
	// 匿名的字符出
	public static final String ANONY_MOUS_USER = "anonymousUser";
	// json编码
	public static final String JSON_TYPE_UTF8 = "application/json;charset=UTF-8";
	
	public static final String PASS_WORD= "123456";
	
	public static final String NULL= "null";
	
	public static final String BEARER="bearer";
	
	public static final String NOT_AUTH_URL_LIST="notAuthUrlList";
}
