package org.spring.boot.fusion.common.tools;

import org.spring.boot.fusion.common.aop.MethodsParameterBean;

public class ThreadLocalTool {

	private static final ThreadLocal<Object> LOCAL = new ThreadLocal<Object>();

	public static void set(Object obj) {
		LOCAL.set(obj);
	}

	public static Object get() {
		return LOCAL.get();
	}
	
	public static <T> T getClass(Class<T> clazz) {
		Object object = LOCAL.get();
		if (object == null)return null;
		return JsonTool.jsonToClass(JsonTool.ObjectToJson(object), clazz);
	}
	
	public static void set(MethodsParameterBean methodsParameterBean) {
		LOCAL.set(methodsParameterBean);
	}

	public static MethodsParameterBean getMethodsParameterBean() {
		MethodsParameterBean object = (MethodsParameterBean) LOCAL.get();
		return object;
	}
}