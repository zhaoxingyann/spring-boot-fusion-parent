package org.spring.boot.fusion.common.exception;

public enum ServiceErrorStatus {
	USER_ISNULL(100001,"用户信息为空"),
	USER_NOTFOUND(100002,"用户信息错误"),
	USER_ISHAVE(100003,"用户已存在"),
    TOKEN_ISNULL(100004,"凭证不能为空"),
    PASSWORD_ERROR(100005,"密码错误"),
    AUTHENTICATION_FAILED(100006,"无效的凭证"),
    NOT_USER(100007,"用户不存在"),
    USER_LOGINS(100008,"用户已经登录"),
    NOT_PERMISSIONS(100009,"没有权限"),
	PARAMETER_ERROR(100010,"参数非法"),
	CREATE_TOKEN_ERROR(100011,"凭证创建异常"),
	REQUEST_ERROR(100012,"请求异常"),
	SEND_MAIL(100013,"邮件发送异常"),
	PHONE_EMAIL_ISNULL(100014,"用户名不能为空"),
	VRIFICATION_ERROR(100015,"验证码错误"),
	MAIL_CONFIG_ISNULL(100016,"邮箱配置为空"),
	MAIL_USERNAME_ISNULL(100017,"邮箱账号为空"),
	MAIL_PASSWORD_ISNULL(100018,"邮箱密码为"),
	MAIL_HOST_ISNULL(100019,"邮箱host为空"),
	METHOD_ERROR(100020,"非法的请求"),
	LOGIN_EXIT(100021,"退出异常"),
	SYSTEM(900000,"系统正在维护"),
	SYS_ERROR(900001,"系统错误");

	
	private int status;

	private String message;
	
	private String describe;

	private ServiceErrorStatus(int status, String message) {
		this.status = status;
		this.message = message;
		this.describe=null;
	}
	
	private ServiceErrorStatus(int status, String message,String describe) {
		this.status = status;
		this.message = message;
		this.describe=describe;
	}

	public int status() {
		return status;
	}

	public String message() {
		return message;
	}
	
	public String describe() {
		return describe;
	}
}
