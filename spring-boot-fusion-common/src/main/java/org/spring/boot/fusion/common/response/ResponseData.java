package org.spring.boot.fusion.common.response;

import java.io.Serializable;
import java.util.HashMap;

import org.spring.boot.fusion.common.exception.ServiceErrorStatus;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseData implements Serializable {

	private static final long serialVersionUID = 6688136731432037929L;
	private Object data;
	private Integer status;
	private String message;
	private String describe;

	private ResponseData() {
	}

	private ResponseData(Object data) {
		this.data = data;
	}

	private ResponseData(ServiceErrorStatus errorStatus) {
		this.status = errorStatus.status();
		this.message = errorStatus.message();
	}

	private ResponseData(Integer status, String message) {
		this.status = status;
		this.message = message;
	}

	private ResponseData(Integer status, String message, String describe) {
		this.status = status;
		this.message = message;
		this.describe = describe;
	}

	public static ResponseData SUCCESS() {
		ResponseData resultData = new ResponseData();
		resultData.setData(new HashMap<>());
		return resultData;
	}

	public static ResponseData SUCCESS(Object data) {
		ResponseData resultData = new ResponseData();
		if(data!=null) {
			resultData.setData(data);
		}else {
			resultData.setData(new HashMap<>());
		}
		return resultData;
	}

	public static ResponseData ERROR(ServiceErrorStatus errorStatus) {
		return new ResponseData(errorStatus);
	}

	public static ResponseData ERROR(Integer status, String message) {
		return new ResponseData(status, message);
	}

	public static ResponseData ERROR(String message) {
		return new ResponseData(HttpStatus.BAD_REQUEST.value(), message);
	}
	
	public static ResponseData ERROR(Integer status, String message, String describe) {
		return new ResponseData(status, message, describe);
	}

	public Object getData() {
		return data;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(Class<T> clazz) {
		T readValue = (T) data;
		return readValue;
	}

}
