package org.spring.boot.fusion.common.tools;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.UUID;

import org.spring.boot.fusion.common.constant.ConstantAttribute;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.util.DigestUtils;

public class TwitterIdTool {

	private static TwitterIdTool twitterId = null;

	static {
		twitterId = new TwitterIdTool();
	}

	/** 开始时间截 (2015-01-01) */
	private final long twepoch = 1420041600000L;

	/** 机器id所占的位数 */
	private final long workerIdBits = 5L;

	/** 数据标识id所占的位数 */
	private final long datacenterIdBits = 5L;

	/** 支持的最大机器id，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数) */
	private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

	/** 支持的最大数据标识id，结果是31 */
	private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

	/** 序列在id中占的位数 */
	private final long sequenceBits = 12L;

	/** 机器ID向左移12位 */
	private final long workerIdShift = sequenceBits;

	/** 数据标识id向左移17位(12+5) */
	private final long datacenterIdShift = sequenceBits + workerIdBits;

	/** 时间截向左移22位(5+5+12) */
	private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

	/** 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095) */
	private final long sequenceMask = -1L ^ (-1L << sequenceBits);

	/** 工作机器ID(0~31) */
	private long workerId;

	/** 数据中心ID(0~31) */
	private long datacenterId;

	/** 毫秒内序列(0~4095) */
	private long sequence = 0L;

	/** 上次生成ID的时间截 */
	private long lastTimestamp = -1L;

	/**
	 * 构造函数
	 * 
	 * @param workerId     工作ID (0~31)
	 * @param datacenterId 数据中心ID (0~31)
	 */
	public TwitterIdTool(long workerId, long datacenterId) {
		if (workerId > maxWorkerId || workerId < 0) {
			throw new IllegalArgumentException(
					String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
		}
		if (datacenterId > maxDatacenterId || datacenterId < 0) {
			throw new IllegalArgumentException(
					String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
		}
		this.workerId = workerId;
		this.datacenterId = datacenterId;
	}

	public TwitterIdTool() {
	};

	/**
	 * 获得下一个ID (该方法是线程安全的)
	 * 
	 * @return SnowflakeId
	 */
	public synchronized long nextId() {
		long timestamp = timeGen();

		// 如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
		if (timestamp < lastTimestamp) {
			throw new RuntimeException(String.format(
					"Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
		}

		// 如果是同一时间生成的，则进行毫秒内序列
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) & sequenceMask;
			// 毫秒内序列溢出
			if (sequence == 0) {
				// 阻塞到下一个毫秒,获得新的时间戳
				timestamp = tilNextMillis(lastTimestamp);
			}
		}
		// 时间戳改变，毫秒内序列重置
		else {
			sequence = 0L;
		}

		// 上次生成ID的时间截
		lastTimestamp = timestamp;

		// 移位并通过或运算拼到一起组成64位的ID
		return ((timestamp - twepoch) << timestampLeftShift) //
				| (datacenterId << datacenterIdShift) //
				| (workerId << workerIdShift) //
				| sequence;
	}

	/**
	 * 阻塞到下一个毫秒，直到获得新的时间戳
	 * 
	 * @param lastTimestamp 上次生成ID的时间截
	 * @return 当前时间戳
	 */
	protected long tilNextMillis(long lastTimestamp) {
		long timestamp = timeGen();
		while (timestamp <= lastTimestamp) {
			timestamp = timeGen();
		}
		return timestamp;
	}

	/**
	 * 返回以毫秒为单位的当前时间
	 * 
	 * @return 当前时间(毫秒)
	 */
	protected long timeGen() {
		return System.currentTimeMillis();
	}

	public static long ID() {
		return twitterId.nextId();
	}

	public static Long Id() {
		long id = TwitterIdTool.ID();
		String valueOf = String.valueOf(id);
		valueOf = valueOf.substring(2, valueOf.length());
		return Long.parseLong(valueOf);
	}

	public static String IdString() {
		String md5DigestAsHex = DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());
		;
		try {
			String localIPList = getLocalIPList();
			String localName = getLocalName();
			String serverNameAndPort = getServerNameAndPort();
			String id = String.valueOf(TwitterIdTool.ID());
			md5DigestAsHex = DigestUtils
					.md5DigestAsHex((localIPList + localName + serverNameAndPort + id + md5DigestAsHex).getBytes());
		} catch (Exception e) {
		}
		return md5DigestAsHex;
	}

	public static String Id256() {
		try {
			MessageDigest instance = MessageDigest.getInstance("SHA-256");
			instance.update(IdString().getBytes("UTF-8"));
			return byte2Hex(instance.digest());
		} catch (Exception e) {

		}
		return null;
	}

	private static String byte2Hex(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		String temp = null;
		for (int i = 0; i < bytes.length; i++) {
			temp = Integer.toHexString(bytes[i] & 0xFF);
			if (temp.length() == 1) {
				stringBuffer.append(UUID.randomUUID().toString().substring(0, 1));
			}
			stringBuffer.append(temp);
		}
		return stringBuffer.toString();
	}

	private static String getLocalIPList() throws Exception {
		StringBuffer stringBuffer = new StringBuffer();
		Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
		while (networkInterfaces.hasMoreElements()) {
			NetworkInterface networkInterface = networkInterfaces.nextElement();
			Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
			while (inetAddresses.hasMoreElements()) {
				InetAddress inetAddress = inetAddresses.nextElement();
				if (inetAddress != null && inetAddress instanceof Inet4Address) {
					stringBuffer.append(inetAddress.getHostAddress());
				}
			}
		}
		return stringBuffer.toString();
	}

	private static String getLocalName() throws Exception {
		InetAddress localHost = InetAddress.getLocalHost();
		return localHost.getHostName();
	}

	private static String getServerNameAndPort() {
		try {
			ServerProperties serverProperties = SpringBeanTool.getBean(ServerProperties.class);
			if (serverProperties == null)
				return ConstantAttribute.NULL;
			String contextPath = serverProperties.getServlet().getContextPath();
			Integer port = serverProperties.getPort();
			if (contextPath != null && port != null) {
				return new StringBuffer().append(contextPath).append(port).toString();
			}
		} catch (Exception e) {
		}
		return ConstantAttribute.NULL;
	}

}