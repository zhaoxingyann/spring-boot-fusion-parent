package org.spring.boot.fusion.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("spring.fusion.aop.log")
public class FusionAopLogProperties {
	
	private boolean logFlag=true;

}
