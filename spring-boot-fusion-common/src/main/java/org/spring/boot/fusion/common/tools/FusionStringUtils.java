package org.spring.boot.fusion.common.tools;

import java.io.File;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpMethod;

public class FusionStringUtils {

	public static Long IDLONG() {
		return TwitterIdTool.ID();
	}

	public static Long IDHTML() {
		return TwitterIdTool.Id();
	}

	public static String IDMD5() {
		return TwitterIdTool.IdString();
	}

	public static String ID256() {
		return TwitterIdTool.Id256();
	}

	public static Map<?, ?> JsonToMap(String jsonString) {
		return JsonTool.JsonToMap(jsonString);
	}

	public static String ObjectToJson(Object object) {
		return JsonTool.ObjectToJson(object);
	}

	public static <T> List<T> jsonToList(String jsonString, Class<T> clazz) {
		return JsonTool.jsonToList(jsonString, clazz);
	}

	public static <T> T jsonToClass(String jsonString, Class<T> clazz) {
		return JsonTool.jsonToClass(jsonString, clazz);
	}

	public static <T> T classToClass(Object bean, Class<T> clazz) {
		return JsonTool.classToClass(bean, clazz);
	}

	public static <T> List<T> ListToList(Object object, Class<T> clazz) {
		return JsonTool.ListToList(object, clazz);
	}

	public static <T> T exchange(Object parameter, String url, HttpMethod httpMethod, Class<T> clazz) {
		return HttpClienTool.exchange(parameter, url, httpMethod, clazz);
	}

	public static <T> T exchange(Object parameter, String url, HttpMethod httpMethod, Class<T> clazz,
			Map<String, String> headers) {
		return HttpClienTool.exchange(parameter, url, httpMethod, clazz, headers);
	}

	public static <T> T exchange(String url, HttpMethod httpMethod, Class<T> clazz) {
		return HttpClienTool.exchange(url, httpMethod, clazz);
	}

	public static <T> T exchange(String url, HttpMethod httpMethod, Class<T> clazz, Map<String, String> headers) {
		return HttpClienTool.exchange(url, httpMethod, clazz, headers);
	}

	public static Object getBean(String name) {
		return SpringBeanTool.getBean(name);
	}

	public static <T> T getBean(Class<T> clazz) {
		return (T) SpringBeanTool.getBean(clazz);
	}

	public static <T> Map<String, T> getBeansOfType(Class<T> clazz) {
		return SpringBeanTool.getBeansOfType(clazz);
	}

	public static String getIpAddr() {
		return IpTool.getIpAddr();
	}

	public static String getIpAddr(HttpServletRequest request) {
		return IpTool.getIpAddr(request);
	}

	public static File urlToFile(String fileUrl) {
		return UrlFileTool.urlToFile(fileUrl);
	}

	public static String getCompareResult(Object beforeSource, Object afterSource) {
		return BeanChangeTool.getCompareResult(beforeSource, afterSource);
	}

}
