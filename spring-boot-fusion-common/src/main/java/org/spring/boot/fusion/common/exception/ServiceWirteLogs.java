package org.spring.boot.fusion.common.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceWirteLogs {

	public static void error(Throwable e) {
		log.error(null, e);
		e.printStackTrace();
	}
}
