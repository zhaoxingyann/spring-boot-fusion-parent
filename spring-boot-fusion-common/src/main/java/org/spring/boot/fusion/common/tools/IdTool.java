package org.spring.boot.fusion.common.tools;

public class IdTool {

	public static Long IDLONG() {
		return TwitterIdTool.ID();
	}

	public static Long IDHTML() {
		return TwitterIdTool.Id();
	}

	public static String IDMD5() {
		return TwitterIdTool.IdString();
	}

	public static String ID256() {
		return TwitterIdTool.Id256();
	}

}
