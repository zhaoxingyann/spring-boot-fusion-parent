package org.spring.boot.fusion.common.constant;

public enum TimeEnum {
	YMDHMS(1,"yyyy-MM-dd HH:mm:ss"),
	YMD(2,"yyyy-MM-dd"),
	HMS(3,"HH:mm:ss"),
	HM(4,"HH:mm");
	
	private int code;

	private String value;

	private TimeEnum(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public int code() {
		return code;
	}

	public String value() {
		return value;
	}
	
}
