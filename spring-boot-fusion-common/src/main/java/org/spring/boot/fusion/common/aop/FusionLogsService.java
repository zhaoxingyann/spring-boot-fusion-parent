package org.spring.boot.fusion.common.aop;

public interface FusionLogsService {

	public void requestLogs(MethodsParameterBean methodsParameterBean);
	
	public Object responseLogs(Object object);
}
