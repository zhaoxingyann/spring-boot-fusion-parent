package org.spring.boot.fusion.common.tools;

import java.util.HashMap;
import java.util.Map;

import com.xkzhangsan.xkbeancomparator.BeanComparator;
import com.xkzhangsan.xkbeancomparator.CompareResult;

public class BeanChangeTool {

	private static final Map<String, String> propertyTranslationMap = new HashMap<>();

	public static String getCompareResult(Object beforeSource, Object afterSource) {
		CompareResult compareResult = BeanComparator.getCompareResult(beforeSource, afterSource,
				propertyTranslationMap);
		if (compareResult.isChanged()) {
			return compareResult.getChangeContent();
		}
		return "There is no change";
	}
}