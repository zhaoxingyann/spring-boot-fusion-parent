package org.springframework.boot.context.logging;

import org.springframework.core.env.ConfigurableEnvironment;

public class LoggingPathXml {

	public static String getLoggingConfig(ConfigurableEnvironment environment) {
		String property = environment.getProperty("logging.level.root");
		String elkPath = environment.getProperty("fusion.elk.path");
		if (elkPath == null || elkPath.trim().length() == 0 || elkPath.equalsIgnoreCase("null")) {

			if (property == null || property.trim().length() == 0 || property.equalsIgnoreCase("info")) {
				return "classpath:logback-local-info.xml";
			}
			if (property != null && property.trim().length() > 0 && property.equalsIgnoreCase("error")) {
				return "classpath:logback-local-error.xml";
			}
			if (property != null && property.trim().length() > 0 && property.equalsIgnoreCase("debug")) {
				return "classpath:logback-local-debug.xml";
			}

		} else if (elkPath != null && elkPath.trim().length() > 0 && !elkPath.equalsIgnoreCase("null")) {

			if (property == null || property.trim().length() == 0 || property.equalsIgnoreCase("null")) {
				return "classpath:logback-elk-error.xml";
			}
			if (property != null && property.trim().length() > 0 && property.equalsIgnoreCase("info")) {
				return "classpath:logback-elk-info.xml";
			}
			if (property != null && property.trim().length() > 0 && property.equalsIgnoreCase("error")) {
				return "classpath:logback-elk-error.xml";
			}
			if (property != null && property.trim().length() > 0 && property.equalsIgnoreCase("debug")) {
				return "classpath:logback-elk-debug.xml";
			}

		}
		return "classpath:logback-local-info.xml";
	}
}
