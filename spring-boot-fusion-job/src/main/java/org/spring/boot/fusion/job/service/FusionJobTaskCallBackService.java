package org.spring.boot.fusion.job.service;
import org.quartz.JobKey;

/**
 * @Description: 任务回调的接口
 * @author: 赵兴炎
 * @date: 2019年7月2日
 */
public interface FusionJobTaskCallBackService{

	/**
	 * @Description: 启动任务回调方法
	 * @param jobKey  
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void startTaskCallBack(JobKey jobKey);
	
	/**
	 * @Description:暂停任务回调方法
	 * @param jobKey  
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void suspendedTasCallBackk(JobKey jobKey);

	/**
	 * @Description:恢复任务回调方法
	 * @param jobKey  
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void restoreTaskCallBack(JobKey jobKey);

	/**
	 * @Description:删除任务回调方法
	 * @param jobKey  
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void deletTaskCallBack(JobKey jobKey);
	
	/**
	 * @Description:修改任务回调方法
	 * @param jobKey  
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void updateTaskCallBack(JobKey jobKey);
}