package org.spring.boot.fusion.job.config;
import java.io.IOException;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * @Description: 设置自定的工厂类，解决类不能注入的问题
 * @author: 赵兴炎
 * @date: 2019年7月2日
 */
@Configuration
@EnableScheduling
public class FusionSchedulerConfig {

	@Autowired
	private FusionJobFactory customJobFactory;

	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() throws IOException {
		SchedulerFactoryBean factory = new SchedulerFactoryBean();
		factory.setOverwriteExistingJobs(true);
		factory.setStartupDelay(200);
		factory.setJobFactory(customJobFactory);
		return factory;
	}

	@Bean
	public Scheduler scheduler() throws Exception {
		return schedulerFactoryBean().getScheduler();
	}

}