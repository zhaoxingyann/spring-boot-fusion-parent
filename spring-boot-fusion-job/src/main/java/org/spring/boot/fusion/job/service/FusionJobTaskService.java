package org.spring.boot.fusion.job.service;

import org.quartz.Job;

import java.util.Map;

public interface FusionJobTaskService {
	
	/**
	 * @Description: 任务启动方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @param clazz 需要执行的任务
	 * @param cron 任务执行规则
	 * @param hashMap 参数接收
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void addJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName, Class<? extends Job> clazz, String cron, Map<String, Object> hashMap);

	/**
	 * @Description: 修改任务方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @param cron 任务执行规则
	 * @param hashMap 参数接收
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void updateTaskCron(String jobName, String jobGroupName, String triggerName, String triggerGroupName, String cron, Map<String, Object> hashMap);

	/**
	 * @Description: 暂停任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 返回值
	 */
	void pauseJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName);

	/**
	 * @Description: 恢复任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void resumeJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName);

	/**
	 * @Description: 删除任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	void removeJob(String jobName, String jobGroupName, String triggerName, String triggerGroupName);
}
