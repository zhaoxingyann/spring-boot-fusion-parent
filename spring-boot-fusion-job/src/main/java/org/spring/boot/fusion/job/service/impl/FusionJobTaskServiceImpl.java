package org.spring.boot.fusion.job.service.impl;

import org.quartz.*;
import org.spring.boot.fusion.common.tools.SpringBeanTool;
import org.spring.boot.fusion.job.service.FusionJobTaskCallBackService;
import org.spring.boot.fusion.job.service.FusionJobTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 定时任务管理实现
 * @author: 赵兴炎
 * @date: 2019年7月2日
 */
@Service
public class FusionJobTaskServiceImpl implements FusionJobTaskService {

	@Resource
	private Scheduler scheduler;
	@Autowired(required = false)
	private FusionJobTaskCallBackService jobTaskCallBackService;

	/**
	 * @Description: 任务启动方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @param cron 任务执行规则
	 * @param hashMap 参数接收
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	@Override
	public void addJob(String jobName, String jobGroupName,String triggerName, String triggerGroupName,Class<? extends Job> clazz, String cron, Map<String, Object> hashMap) {
		try {
			JobDataMap jobDataMap = null;
			if (hashMap != null && hashMap.size() > 0) {
				jobDataMap = new JobDataMap(hashMap);
			} else {
				jobDataMap = new JobDataMap(new HashMap<>());
			}
			Map<String, Job> beansOfType = SpringBeanTool.getBeansOfType(Job.class);
			JobDetail jobDetail = JobBuilder.newJob(beansOfType.get(jobName).getClass()).withIdentity(jobName, jobGroupName).build();
			CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerName, triggerGroupName).usingJobData(jobDataMap)
					.withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
			scheduler.scheduleJob(jobDetail, cronTrigger);
			scheduler.triggerJob(new JobKey(jobName, jobGroupName));
			if (jobTaskCallBackService != null) {
				jobTaskCallBackService.startTaskCallBack(jobDetail.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Description: 修改任务方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @param cron 任务执行规则
	 * @param hashMap 参数接收
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	@Override
	public void updateTaskCron(String jobName, String jobGroupName,String triggerName, String triggerGroupName,String cron, Map<String, Object> hashMap) {
		JobDataMap jobDataMap = null;
		if (hashMap != null && hashMap.size() > 0) {
			jobDataMap = new JobDataMap(hashMap);
		} else {
			jobDataMap = new JobDataMap(new HashMap<>());
		}
		try {
			TriggerKey triggerKey = new TriggerKey(jobName, jobGroupName);
			CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron);
			CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName, triggerGroupName).usingJobData(jobDataMap)
					.withSchedule(cronScheduleBuilder).build();
			scheduler.rescheduleJob(triggerKey, trigger);
			if (jobTaskCallBackService != null) {
				jobTaskCallBackService.updateTaskCallBack(new JobKey(jobName,jobGroupName));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Description: 暂停任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 返回值
	 */
	@Override
	public void pauseJob(String jobName, String jobGroupName,String triggerName, String triggerGroupName) {
		try {
			JobKey jobKey = new JobKey(jobName, jobGroupName);
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
			scheduler.pauseTrigger(triggerKey);
			scheduler.pauseJob(jobKey);
			if (jobTaskCallBackService != null) {
				jobTaskCallBackService.suspendedTasCallBackk(new JobKey(jobName,jobGroupName));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Description: 恢复任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	@Override
	public void resumeJob(String jobName, String jobGroupName,String triggerName, String triggerGroupName) {
		try {
			JobKey jobKey = new JobKey(jobName, jobGroupName);
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
			scheduler.resumeTrigger(triggerKey);
			scheduler.resumeJob(jobKey);
			if (jobTaskCallBackService != null) {
				jobTaskCallBackService.restoreTaskCallBack(new JobKey(jobName,jobGroupName));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @Description: 删除任务的方法
	 * @param jobName 任务名字（唯一标识，对应spring容器中类的名字）
	 * @param jobGroupName 任务组的名字
	 * @param triggerName 触发器名称
	 * @param triggerGroupName 出发器组名称
	 * @author: 赵兴炎
	 * @date: 2019年7月2日
	 * @return: 无
	 */
	@Override
	public void removeJob(String jobName, String jobGroupName,String triggerName, String triggerGroupName) {
		try {
			JobKey jobKey = new JobKey(jobName, jobGroupName);
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
			scheduler.pauseTrigger(triggerKey);
			scheduler.unscheduleJob(triggerKey);
			scheduler.deleteJob(jobKey);
			if (jobTaskCallBackService != null) {
				jobTaskCallBackService.deletTaskCallBack(new JobKey(jobName,jobGroupName));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
