package org.spring.boot.fusion.swagger.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("spring.fusion.swagger")
public class FusionSwaggerProperties {

	private String webPackagePath;

	public String getWebPackagePath() {
		return webPackagePath;
	}

	public void setWebPackagePath(String webPackagePath) {
		this.webPackagePath = webPackagePath;
	}
}
