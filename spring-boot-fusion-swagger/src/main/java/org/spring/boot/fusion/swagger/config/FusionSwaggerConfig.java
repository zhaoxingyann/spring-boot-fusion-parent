package org.spring.boot.fusion.swagger.config;

import java.util.ArrayList;
import java.util.List;

import org.spring.boot.fusion.swagger.properties.FusionSwaggerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;

@Configuration
@EnableSwagger2
public class FusionSwaggerConfig {

	@Autowired
	private FusionSwaggerProperties gemFrameSwaggerProperties;

	@Bean
	public Docket createRestApi() {
		String webPackage = gemFrameSwaggerProperties.getWebPackagePath();
		if (webPackage == null || webPackage.equalsIgnoreCase("null") || webPackage.length() == 0) {
			webPackage = "null";
		}
		return new Docket(DocumentationType.SWAGGER_2).globalOperationParameters(setHearderParams()).apiInfo(apiInfo())
				.select().apis(RequestHandlerSelectors.basePackage(webPackage)).paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().build();
	}

	private List<Parameter> setHearderParams() {
		List<Parameter> arrayList = new ArrayList<Parameter>();
		ParameterBuilder tokenPar = new ParameterBuilder();
		tokenPar.name("Authorization");
		tokenPar.description("认证凭证");
		tokenPar.modelRef(new ModelRef("string"));
		tokenPar.parameterType("header");
		tokenPar.required(false);
		tokenPar.build();
		arrayList.add(tokenPar.build());
		return arrayList;
	}
}
